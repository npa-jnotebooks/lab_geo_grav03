# LAB_GEO_GRAV03

Jupyter Notebook interattivo per modellare l' anomalia di gravita' dovuta al Corpo di Ivrea

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_grav03/HEAD?labpath=UNIMIB_GEO_GRAV03_ivrea.ipynb)

